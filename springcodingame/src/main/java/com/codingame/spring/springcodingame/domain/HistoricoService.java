package com.codingame.spring.springcodingame.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HistoricoService {

    @Autowired
    private HistoricoRepository rep;

    public Iterable<Historico> getHistorico() {
        return rep.findAll();
    }

    public Optional<Historico> getHistoricoById(Long id) {
        return rep.findById(id);
    }

    public List<Historico> getHistoricoByOperacao(String operacao) {
        return rep.findByOperacao(operacao);
    }

    public Historico sum(Historico calc) {
        calc.setResultado( calc.getValor1() + calc.getValor2());
        calc.setOperacao("+");
      return rep.save(calc);
    }

    public Historico sub(Historico calc) {
        calc.setResultado(calc.getValor1() - calc.getValor2());
        calc.setOperacao("-");
        return rep.save(calc);
    }

    public Historico multi(Historico calc) {
        calc.setResultado(calc.getValor1() * calc.getValor2());
        calc.setOperacao("*");
        return rep.save(calc);
    }

    public Historico div(Historico calc) {
        calc.setResultado(calc.getValor1() / calc.getValor2());
        calc.setOperacao("/");
        return rep.save(calc);
    }

    public Historico pot(Historico calc) {
        calc.setResultado(Math.pow(calc.getValor1(), calc.getValor2()));
        calc.setOperacao("^");
        return rep.save(calc);
    }
}
