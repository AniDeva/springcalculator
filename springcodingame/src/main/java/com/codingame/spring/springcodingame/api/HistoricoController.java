package com.codingame.spring.springcodingame.api;

import com.codingame.spring.springcodingame.domain.Historico;
import com.codingame.spring.springcodingame.domain.HistoricoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class HistoricoController {

    @Autowired
    private HistoricoService service;

    @GetMapping
    public String get() {
        return "============ CALCULATOR ===========";
    }

    @GetMapping(value = "/historico")
    public ResponseEntity<Iterable<Historico>> getHistorico() {
        return ResponseEntity.ok(service.getHistorico());
    }

    @GetMapping(value ="/{id}")
    public ResponseEntity<Historico> getById(@PathVariable("id") Long id) {
        Optional<Historico> historico = service.getHistoricoById(id);

        return historico.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/operador/{operador}")
    public ResponseEntity getHistoricoByOperador(@PathVariable ("operador") String operacao) {
        List<Historico> historicos = service.getHistoricoByOperacao(operacao);

        return historicos.isEmpty() ?
                ResponseEntity.noContent().build():
                ResponseEntity.ok(historicos);
    }

    @PostMapping("/sum")
    public String soma(@RequestBody Historico calc) {
        Historico soma = service.sum(calc);

        return "Adição Id:  " + soma.getId() + " | " + soma.getValor1() + " + " + soma.getValor2() + " = " + soma.getResultado();
    }

    @PostMapping("/sub")
    public String sub(@RequestBody Historico calc) {
        Historico sub = service.sub(calc);
        return "Subtração Id:  " + sub.getId() + " | " + sub.getValor1() + " - " + sub.getValor2() + " = " + sub.getResultado();
    }

    @PostMapping("/multiply")
    public String multi(@RequestBody Historico calc) {
        Historico multi = service.multi(calc);
        return "Multiplicação Id:  " + multi.getId() + " | " + multi.getValor1() + " * " + multi.getValor2() + " = " + multi.getResultado();
    }

    @PostMapping("/division")
    public String division(@RequestBody Historico calc) {
        Historico div = service.div(calc);
        return "Divisão Id:  " + div.getId() + " | " + div.getValor1() + " / " + div.getValor2() + " = " + div.getResultado();
    }

    @PostMapping("/pow")
    public String div(@RequestBody Historico calc) {
        Historico pot = service.pot(calc);
        return "Potenciação Id:  " + pot.getId() + " | " + pot.getValor1() + " ^ " + pot.getValor2() + " = " + pot.getResultado();
    }
}
