package com.codingame.spring.springcodingame.domain;

import javax.persistence.*;

@Entity
public class Historico {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private double valor1;
    private String operacao;
    private double valor2;
    private double resultado;


    public Historico() {    }

    public Historico(double valor1, String operacao, double valor2, double resultado) {
        this.valor1 = valor1;
        this.operacao = operacao;
        this.valor2 = valor2;
        this.resultado = resultado;
    }

    public Long getId() {
        return id;
    }

    public double getValor1() {
        return valor1;
    }

    public void setValor1(double valor1) {
        this.valor1 = valor1;
    }

    public double getValor2() {
        return valor2;
    }

    public void setValor2(double valor2) {
        this.valor2 = valor2;
    }

    public String getOperacao() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao = operacao;
    }

    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }
}
