package com.codingame.spring.springcodingame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcodingameApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcodingameApplication.class, args);
	}

}
