package com.codingame.spring.springcodingame.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface HistoricoRepository extends CrudRepository<Historico, Long> {
    List<Historico> findByOperacao(String operacao);

}
