Para utilizar a o SpringCalculator será necessária a ferramenta Postman:

Para dar inicio ao Spring Calculator
Com o metodo GET escreva: localhost:8080/ 

Para efetuar os calcualos: 
--- SOMA ---
Com o metodo GET escreva: localhost:8080/calculadora/sum

No header com a Key: Content-Type e o value application/json
No body escreva os valores a serem calculados conforme exemplo:

{
        "valor1": 10.0,
        "valor2": 13.5
        
    }
	
Apert SEND

--- SUBTRAIR ---
Com o metodo GET escreva: localhost:8080/calculadora/sub

No header com a Key: Content-Type e o value application/json
No body escreva os valores a serem calculados conforme exemplo:

{
        "valor1": 10.0,
        "valor2": 13.5
        
    }

--- MULTIPLICAR ---
Com o metodo GET escreva: localhost:8080/calculadora/multiply

No header com a Key: Content-Type e o value application/json
No body escreva os valores a serem calculados conforme exemplo:

{
        "valor1": 10.0,
        "valor2": 13.5
        
    }
	
Apert SEND

--- DIVIDIR ---
Com o metodo GET escreva: localhost:8080/calculadora/division

No header com a Key: Content-Type e o value application/json
No body escreva os valores a serem calculados conforme exemplo:

{
        "valor1": 10.0,
        "valor2": 13.5
        
    }
	
Apert SEND

--- POTENCIAR ---
Com o metodo GET escreva: localhost:8080/calculadora/pow

No header com a Key: Content-Type e o value application/json
No body escreva os valores a serem calculados conforme exemplo:

{
        "valor1": 10.0,
        "valor2": 13.5
        
    }
	
Apert SEND

--- PARA VER O HISTÓRICO DE CALCULOS ---
Com o metodo GET escreva: localhost:8080/calculadora/historico

Apert SEND

Obrigada por calcular conosco! Agradecemos a preferência!
	